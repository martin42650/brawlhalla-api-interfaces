import { IClanMember } from './IClanMember';

export interface IClan {
  name: string;
  level: number;
  xp: number;
  creationDate: number;
  members: IClanMember[];
}
