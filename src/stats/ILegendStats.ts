import { ILegend } from '../ILegend';

export interface ILegendStats {
  legend: ILegend;

  xp: number;
  level: number;
  xpPercentage: number;

  damageDealt: number;
  damageTaken: number;
  kos: number;
  falls: number;
  suicides: number;
  teamKos: number;
  matchTime: number;
  games: number;
  wins: number;

  defeats: number;

  // Damage
  damageDealtByUnarmed: number;
  damageDealtByThrownItems: number;
  damageDealtByFirstWeapon: number;
  damageDealtBySecondWeapon: number;
  damageDealtByGadgets: number;

  // Kos
  koByUnarmed: number;
  koByThrownItems: number;
  koByFirstWeapon: number;
  koBySecondWeapon: number;
  koByGadgets: number;

  timeHeldFirstWeapon: number;
  timeHeldSecondWeapon: number;
}
