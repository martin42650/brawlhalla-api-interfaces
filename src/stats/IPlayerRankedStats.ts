import { ILegendRankedStats } from './ILegendRankedStats';
import { IPlayerTeamStats } from './IPlayerTeamStats';
import { TValidDivision, TValidRegions, TValidTier } from '../types';

export interface IPlayerRankedStats {
  brawlhallaID: number;
  name: string;

  region: TValidRegions;
  globalRank: number;
  regionRank: number;

  rating: number;
  peakRating: number;
  tier: TValidTier;
  division: TValidDivision;

  legends: ILegendRankedStats[];
  teams: IPlayerTeamStats[];
}
