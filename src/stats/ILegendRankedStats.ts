import { ILegend } from '../ILegend';
import { TValidDivision, TValidTier } from '../types';

export interface ILegendRankedStats {
  legend: ILegend;
  rating: number;
  peakRating: number;

  tier: TValidTier;
  division: TValidDivision;

  games: number;
  wins: number;
  defeats: number;
}
