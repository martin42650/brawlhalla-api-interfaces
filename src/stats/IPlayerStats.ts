import { ISimpleClan } from '../clan/ISimpleClan';
import { ILegendStats } from './ILegendStats';
import { IWeaponStats } from './IWeaponStats';

export interface IPlayerStats {
  brawlhallaID: number;
  name: string;
  xp: number;
  level: number;
  xpPercentage: number;
  games: number;
  wins: number;

  // Damage
  damageDealtByBomb: number;
  damageDealtByMine: number;
  damageDealtBySpikeball: number;
  damageDealtBySidekick: number;

  snowballHits: number;

  // Kos
  koByBomb: number;
  koByMine: number;
  koBySpikeball: number;
  koBySidekick: number;
  koBySnowball: number;

  legendStats: ILegendStats[];
  weaponStats: IWeaponStats[];
  clan?: ISimpleClan;
}
