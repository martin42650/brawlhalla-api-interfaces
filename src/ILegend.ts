import { IWeapon } from './IWeapon';
import { TLegendName } from './types';

// TODO: Add description, lore, stats, skins, etc.
export interface ILegend {
  id: number;
  name: TLegendName;
  firstWeapon: IWeapon;
  secondWeapon: IWeapon;
}
