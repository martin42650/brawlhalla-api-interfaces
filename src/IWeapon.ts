import { TWeaponName } from './types';

export interface IWeapon {
  id: number;
  name: TWeaponName;
}
